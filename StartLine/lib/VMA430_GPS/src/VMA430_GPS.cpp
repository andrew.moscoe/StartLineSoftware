/*************************************************** 

 ****************************************************/

#include "VMA430_GPS.h"
#include "error_check.h"
#ifdef __AVR__
#include <util/delay.h>
#include <SoftwareSerial.h>
#endif

//TODO: DISABLE SBAS

#ifdef __AVR__
VMA430_GPS::VMA430_GPS(SoftwareSerial *ss)
{
    swSerial = ss;
    stream = ss;
}
#endif

VMA430_GPS::VMA430_GPS(HardwareSerial *ss)
{
    hwSerial = ss;
    stream = ss;
}

int VMA430_GPS::begin()
{
    Serial1.begin(9600);
    //disable unwanted messages
    
    char setGLL[] = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x2B};
    char setGSA[] = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x32};
    char setGSV[] = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x39};
    char setRMC[] = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x04, 0x40};
    char setVTG[] = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x04, 0x46};
    char setGGA[] = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00};
    char cfgSBAS[] = {0xB5, 0x62, 0x06, 0x16, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; //turn off SBAS
    Serial1.write(0xFF); //wake up
    Serial1.write(0xFF);
    delay(1);
    calcChecksum(&setGSV[2], 12);
    calcChecksum(&setGGA[2], 12);
    calcChecksum(&setVTG[2], 12);
    calcChecksum(&cfgSBAS[2], 13);
    sendUBX(setGLL, 16);
    delayMicroseconds(500);
    sendUBX(setGSA, 16);
    delayMicroseconds(500);
    sendUBX(setGSV, 16);
    delayMicroseconds(500);
    sendUBX(setRMC, 16);
    delayMicroseconds(500);
    sendUBX(setVTG, 16);
    delayMicroseconds(500);
    sendUBX(setGGA, 16);
    delayMicroseconds(500);
    sendUBX(cfgSBAS, 17);
    delayMicroseconds(500);
    enableAssistNow();
    return 0;
}

/**
 * @brief gets time of the last time mark from the GPS
 * 
 * @param weeks GPS week number of last time mark, or -1 if error
 * @param milliseconds GPS milliseconds of week, or -1 if error
 */
void VMA430_GPS::getTimePulse()
{   

    char getTM2[] = {0xB5, 0x62, 0x0D, 0x01, 0x00, 0x00, 0x00, 0x00};
    int err;
    uint32_t timeout;
    char message[24];
    memset(message, 0, 24);

    calcChecksum(&getTM2[2], 4);

    //before sending, clear the read buffer
    while(Serial1.available()) {
        Serial1.read();
    }
    sendUBX(getTM2, 8);

    timeout = millis();
    while (!Serial1.available() && millis() - timeout < 2);

    //capture 28+8 bytes
    err = Serial1.readBytes(message, 24);
    if (err == 0)
    {
        return; //case where no bytes are read
    }

    weeks = ((uint32_t)message[12+6]) + ((uint32_t)message[13+6] << 8);
    ms = ((uint32_t)message[6]) + ((uint32_t)message[7] << 8) + ((uint32_t)message[8] << 16) + ((uint32_t)message[9] << 24);
}

void VMA430_GPS::enableAssistNow()
{
    char enableAssistNow[48];
    memset(enableAssistNow, 0, 48);
    enableAssistNow[0] = 0xB5;
    enableAssistNow[1] = 0x62;
    enableAssistNow[2] = 0x06;
    enableAssistNow[3] = 0x23;
    enableAssistNow[4] = 40;

    enableAssistNow[8] = 0x00;
    enableAssistNow[9] = 0x40; //mask 2^14

    enableAssistNow[33] = 1; //assistnow enabled

    enableAssistNow[36] = 0xFF; //assistnow error in metres

    calcChecksum(&enableAssistNow[2], 44);

    sendUBX(enableAssistNow, 48);
}

void VMA430_GPS::sendUBX(char *UBXmsg, char msgLength)
{
    for (int i = 0; i < msgLength; i++)
    {
        Serial1.write(UBXmsg[i]);
    }
    Serial1.flush();
}

void VMA430_GPS::powersave() {
    //UBX-CFG-PM2 to configure power save settings
    //Set to only wake up on external interrupt
    //set to wake up if needed to get new ephemeris data
    char ubxPM2[] = {0xB5, 0x62, 0x06, 0x3B, 0x2C, 0x00, 0x01, 0x06, 0x00, 0x00, 0x0E, 0x94, 0x41, 0x01, 0x00, 0x00, 0x00, 0x00, 0xE8, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2C, 0x01, 0x00, 0x00, 0x4F, 0xC1, 0x03, 0x00, 0x87, 0x02, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x64, 0x40, 0x01, 0x00, 0xB0, 0x8F};

    sendUBX(ubxPM2, 52);

    delay(50); //DEBUG
    //UBX-CFG-RXM to enable power save mode
    char ubxRXM[] = {0xB5, 0x62, 0x06, 0x11, 0x02, 0x00, 0x08, 0x01, 0x22, 0x92};
    sendUBX(ubxRXM, 10);
}

int VMA430_GPS::checkUBX(char *buf, int length)
{
    char CK_A = 0, CK_B = 0;
    int i;
    //do checksum
    for (i = 2; i < length - 2; i++)
    {
        CK_A = CK_A + buf[i];
        CK_B = CK_B + CK_A;
    }
    if (buf[length - 2] == CK_A && buf[length - 1] == CK_B)
    {
        return 0;
    }
    else
    {
        return ERR_UBX_CHECKSUM;
    }
}

int VMA430_GPS::getUBX_ACK(char *msgID)
{
    byte CK_A = 0, CK_B = 0;
    byte incoming_char;
    unsigned long ackWait = millis();
    byte ackPacket[10] = {0xB5, 0x62, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    int i = 0;
    while (1)
    {
        if (stream->available())
        {
            incoming_char = stream->read();
            if (incoming_char == ackPacket[i])
            {
                i++;
            }
            else if (i > 2)
            {
                ackPacket[i] = incoming_char;
                i++;
            }
        }
        if (i > 3)
            break;
        if ((millis() - ackWait) > 1500)
        {
            return ERR_UBX_TIMEOUT;
        }
        if (i == 4 && ackPacket[3] == 0x00)
        {
            return ERR_UBX_NAK;
        }
    }

    for (i = 2; i < 8; i++)
    {
        CK_A = CK_A + ackPacket[i];
        CK_B = CK_B + CK_A;
    }
    if (msgID[0] == ackPacket[6] && msgID[1] == ackPacket[7] && CK_A == ackPacket[8] && CK_B == ackPacket[9])
    {
        return 0;
    }
    else
    {
        return ERR_UBX_CHECKSUM;
    }
}

void VMA430_GPS::calcChecksum(char *checksumPayload, char payloadSize)
{
    char CK_A = 0, CK_B = 0;
    for (int i = 0; i < payloadSize; i++)
    {
        CK_A = CK_A + *checksumPayload;
        CK_B = CK_B + CK_A;
        checksumPayload++;
    }
    *checksumPayload = CK_A;
    checksumPayload++;
    *checksumPayload = CK_B;
}