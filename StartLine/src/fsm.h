#ifndef FSM
#define FSM

#include "StateMachine.h"
#include "error_check.h"
#include "images.h"
#include "epaper.h"
#include "global_defs.h"
#include "error_check.h"

class RaceTimer : public StateMachine
{
public:
	RaceTimer() : StateMachine(ST_MAX_STATES) {}

	// External events taken by this state machine
    void GPSTimeAcquired();
    void StartRace();
	void StopRace();
	void StartStart_f();
	void StopStart_f();
	void Tap();
	void Wand();
	int interval = 0; //TODO LATER
	int startbeeps = 0; //TODO LATER
	uint32_t gps_weeks = 0;
	uint32_t wand_ms = 0;
	uint32_t gps_ms_offset = 0;
	bool wandInterruptEnabled = false;
	int bib = 0;
    
private:
	//helper arrays for numbers
	const char* numR_array[10] = {num0r, num1r, num2r, num3r, num4r, num5r, num6r, num7r, num8r, num9r};
	const char* numL_array[10] = {num0l, num1l, num2l, num3l, num4l, num5l, num6l, num7l, num8l, num9l};

    //state machine state functions
    void ST_WaitGPS(EventData*);
    void ST_Disconnected(EventData*);
    void ST_EnableTag(EventData*);
    void ST_WaitingTap(EventData*);
    void ST_StopStart(EventData*);
    void ST_ReadyGo(EventData*);
    void ST_Triggered(EventData*);
    void ST_StopStartGo(EventData*);

    // State map to define state object order.
	BEGIN_STATE_MAP
        STATE_MAP_ENTRY(&RaceTimer::ST_WaitGPS)
		STATE_MAP_ENTRY(&RaceTimer::ST_Disconnected)
		STATE_MAP_ENTRY(&RaceTimer::ST_EnableTag)
		STATE_MAP_ENTRY(&RaceTimer::ST_WaitingTap)
		STATE_MAP_ENTRY(&RaceTimer::ST_StopStart)
		STATE_MAP_ENTRY(&RaceTimer::ST_ReadyGo)
        STATE_MAP_ENTRY(&RaceTimer::ST_Triggered)
        STATE_MAP_ENTRY(&RaceTimer::ST_StopStartGo)
	END_STATE_MAP

	// State enumeration order must match the order of state method entries
	// in the state map.
	enum E_States
	{
        ST_WAITGPS,
		ST_DISCONNECTED,
		ST_ENABLETAG,
		ST_WAITINGTAP,
        ST_STOPSTART,
		ST_READYGO,
		ST_TRIGGERED,
        ST_STOPSTARTGO, //stop start but remember the last bib
		ST_MAX_STATES
	};

	
};

#endif