#ifndef ERROR_CHECK
#define ERROR_CHECK
#include "Particle.h"



#define ERRCHK(x) do { \
    if(x != 0) { \
        Log.error("Error %d at line %d", x, __LINE__); \
    } \
} while(0)


#define ERRCHKRTN(x) do { \
    if(x != 0) { \
        Log.error("Error %d at line %d", x, __LINE__); \
        return x; \
    } \
} while(0)

#define ERR_UBX_CHECKSUM 1
#define ERR_UBX_TIMEOUT 2
#define ERR_UBX_NAK 3
#define ERR_ASSISTNOW_TIMEOUT 4
#define ERR_LARGE_DOWNLOAD 5

void fatalerror(int code);

#endif