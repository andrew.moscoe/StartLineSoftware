#include "epaper.h"
#include <SPI.h>
#include "Particle.h"

//TODO: SPLIT LOADING LUT AND SWITCHING LUT FUNCTIONS
//LOAD FAST LUT TO RAM ON BOOT
//SWITCHING FUNCTIONS JUST SWITCH

Epaper::Epaper(int busyPin, int resetPin, int dataCommandPin, int chipSelect, int timeoutms) {
    busy = busyPin;
    reset = resetPin;
    dc = dataCommandPin;
    cs = chipSelect;
    timeout = timeoutms;
}

void Epaper::initIO() {
    pinMode(busy, INPUT);
    pinMode(reset, OUTPUT);
    pinMode(dc, OUTPUT);
    pinMode(cs, OUTPUT);
    SPI.begin(cs);
    SPI.setClockSpeed(100, KHZ);
    memset(lastimagebuffer, 0xFF, 2888);
}

/**
 * @brief Powerup function
 * Does not include setting vcom and data interval
 * @return int 0 on success, else error
 */
int Epaper::powerup() {
    digitalWrite(reset, LOW);
    delay(10);
    digitalWrite(reset, HIGH);
    delay(10);

    //BOOST SOFT START
    sendCmd(0x06);
    sendData(0x17);
    sendData(0x17);
    sendData(0x17);

    //power on
    sendCmd(0x04);
    
    //wait for ready signal
    if(lcdCheck() != 0) {
        return -1;
    }

    //panel setting
    sendCmd(0x00);
    sendData(0x1F);

    //resolution setting 152x152
    sendCmd(0x61);
    sendData(0x98);
    sendData(0x00);
    sendData(0x98); 

    return 0;
}

/**
 * @brief Sets controller to deep sleep mode
 * 
 * @return int 0
 */
int Epaper::sleep() {
    sendCmd(0x02);
    sendCmd(0x07);
    sendData(0xA5);
    return 0;
}

int Epaper::sendImage(char* image) {
    return sendImage(lastimagebuffer, image);
}



int Epaper::sendImage(const char* image) {
    return sendImage(lastimagebuffer, image);
}


/**
 * @brief send full screen image
 * 
 * @param newData must be 2888 bytes
 * @param oldData must be 2888 bytes
 * @return int 0 success, -1 on failure
 */
int Epaper::sendImage(char* olddata, const char *newdata) {
    int i;

    //set vcom and data interval and frame
    sendCmd(0x50);
    sendData(0x97);

    //send old data
    sendCmd(0x10);
    for(i = 0; i < 2888; i++) {
        sendData(olddata[i]);
    }
    
    //send new data
    sendCmd(0x13);
    for(i = 0; i < 2888; i++) {
        sendData(newdata[i]);
    }
    
    //refresh display
    sendCmd(0x12);
    
    //vcom and data interval setting
    sendCmd(0x50);
    sendData(0xF7);

    //copy new image to old image
    memcpy(lastimagebuffer, newdata, 2888);
    return 0;
}


int Epaper::sendImage(char* olddata, char *newdata) {
    int i;

    //set vcom and data interval and frame
    sendCmd(0x50);
    sendData(0x97);

    //send old data
    sendCmd(0x10);
    for(i = 0; i < 2888; i++) {
        sendData(olddata[i]);
    }
    
    //send new data
    sendCmd(0x13);
    for(i = 0; i < 2888; i++) {
        sendData(newdata[i]);
    }
    
    //refresh display
    sendCmd(0x12);
    
    //vcom and data interval setting
    sendCmd(0x50);
    sendData(0xF7);
    //copy new image to old image
    memcpy(lastimagebuffer, newdata, 2888);
    return 0;
}



int Epaper::fullRefresh() {
    int templut = lutstatus;
    setOTPLUT();
    
    int i;
    //set vcom and data interval
    sendCmd(0x50);
    sendData(0x97);

    //send old data
    sendCmd(0x10);
    for(i = 0; i < 2888; i++) {
        sendData(lastimagebuffer[i]);
    }
    
    //send white data
    sendCmd(0x13);
    for(i = 0; i < 2888; i++) {
        sendData(0xFF);
    }
    
    //refresh display
    sendCmd(0x12);
    if(lcdCheck() != 0) {
        return -1;
    }
    
    //vcom and data interval setting
    sendCmd(0x50);
    sendData(0xF7);
    
    //return LUT settings
    if(templut == 1) {
        setFastLUT();
    }

    //set last screen buffer to white
    memset(lastimagebuffer, 0xFF, 2888);
    return 0;
}

void Epaper::setFastLUT() {
    lutstatus = 1;
    sendCmd(0x00); //panel setting
    sendData(0xBF); //LUT from OTP
    sendData(0x0D); //VCOM to 0V fast

    sendCmd(0x30); //PLL setting
    sendData(0x3A); //100Hz

    sendCmd(0x61);
    sendData(0x98);
    sendData(0x00);
    sendData(0x98);

    sendCmd(0x82); //vcom_dc TODO PLAY AROUND WITH THIS
    sendData(0x08);
    loadFastLUT();

}

void Epaper::loadFastLUT() {
     //load LUT
    int count;
    sendCmd(0x20);
	for(count=0;count<44;count++)	     
		{sendData(lut_vcom1[count]);}

	sendCmd(0x21);
	for(count=0;count<42;count++)	     
		{sendData(lut_ww1[count]);}   
	
	sendCmd(0x22);
	for(count=0;count<42;count++)	     
		{sendData(lut_bw1[count]);} 

	sendCmd(0x23);
	for(count=0;count<42;count++)	     
		{sendData(lut_wb1[count]);} 

	sendCmd(0x24);
	for(count=0;count<42;count++)	     
		{sendData(lut_bb1[count]);}   
}

/**
 * @brief Sets the controller to use the default waveform LUT
 * 
 * 
 */
void Epaper::setOTPLUT() {
    lutstatus = 0;
    //panel setting - use default LUT
    sendCmd(0x00);
    sendData(0x1F);

    //resolution setting
    sendCmd(0x61);
    sendData(0x98);
    sendData(0x00);
    sendData(0x98);

    //VCOM setting to default
    sendCmd(0x82);
    sendCmd(0x00);
}

/**
 * @brief Requests busy pin update, waits for busy pin to go low
 * 
 * @return int 0 on success, -1 if timeout
 */
int Epaper::lcdCheck() {
    delayMicroseconds(500);
    sendCmd(0x71);
    unsigned int starttime = millis();
    unsigned int timedelta = 0;
    int del;
    if(lutstatus == 0) {
        del = 675; //OTP LUT has 6750ms delay usually
    } else {
        del = 50;
    }
    while(digitalRead(busy) == 0 && timedelta < timeout) {
        timedelta = millis() - starttime;
        delay(del);
    }
    if(timedelta < timeout) {
        return 0;
    } else {
        return -1;
    }
}

void Epaper::sendCmd(char cmd) {
    digitalWrite(cs, LOW);
    digitalWrite(dc, LOW);
    SPI.transfer(cmd);
    digitalWrite(dc, HIGH);
    digitalWrite(cs, HIGH);
}

void Epaper::sendData(char data) {
    digitalWrite(cs, LOW);
    SPI.transfer(data);
    digitalWrite(cs, HIGH);
}
