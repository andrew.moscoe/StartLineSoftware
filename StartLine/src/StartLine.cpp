/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "Particle.h"
#line 1 "/home/andrew/Documents/StartLineSoftware/StartLine/src/StartLine.ino"
/*
 * Project StartLine
 * Description:
 * Author:
 * Date:
 */

//TODO: ON POWEROFF SEND COMMANDS TO BLANK EPAPER DISPLAY?
//TODO: FIX GPS

#include "epaper.h"
#include <string>
#include "error_check.h"
#include "fsm.h"
#include "images.h"
#include "global_defs.h"
#include <SPI.h>
#include <MFRC522.h>
#include "VMA430_GPS.h"
#include "images.h"

void tagReaderTimerCallback();
int serverSettingsCallback(String command);
void setGPSOffset();
void gpsPPSCallback();
void wandInterrupt();
void powerButtonTimerCallback();
void powerButtonInterrupt();
void powerOff();
void dispBatteryChg(int charge);
void setup();
void loop();
#line 22 "/home/andrew/Documents/StartLineSoftware/StartLine/src/StartLine.ino"
SYSTEM_MODE(SEMI_AUTOMATIC)
SYSTEM_THREAD(ENABLED)

//FILE GLOBALS
static volatile bool startevent = false; //Used to trigger sending a time to the server when there is a start event
static RaceTimer fsm;                    //container for the racetimer finite state machine
MFRC522 mfrc522(SS_PIN, RST_PIN);        // Create MFRC522 instance
static bool tagReaderEnabled = false;
static bool tagReaderTimerFlag;
static bool powerOffFlag = false;
Timer tagReaderTimer(200, tagReaderTimerCallback, true);
Timer powerButtonTimer(50, powerButtonTimerCallback, false);
static int bibtemp;
static int powerOffCycleCount = 0; //used to debounce the power off button
static bool chargingModeFlag = false;

//GPS
static volatile int gpstimefix = 0; //flag for GPS time set or not
static VMA430_GPS gps(&Serial1);
static uint32_t rtcOffset;
static bool gpstimeset = false;

//GLOBAL GLOBALS (from global_defs.h)
Epaper *epd;

void tagReaderTimerCallback()
{
    tagReaderTimerFlag = true;
}

int serverSettingsCallback(String command)
{
    //TODO:
    //Race start/stop
    //Start start/start stop
    Log.info("Command: %s", command.c_str());

    if (command.compareTo("startrace") == 0)
    {
        //Race start
        Log.info("Server race start");
        fsm.StartRace();
        //ENABLE TAG READER
        tagReaderEnabled = true;
        tagReaderTimer.start();
    }
    else if (command.compareTo("stoprace") == 0)
    {
        //Race stop
        Log.info("server race start");
        fsm.StopRace();
        //DISABLE TAG READER
        tagReaderEnabled = false;
    }
    else if (command.compareTo("startstart") == 0)
    {
        //start start
        Log.info("server start start");
        fsm.StartStart_f();
        //ENABLE TAG READER
        tagReaderEnabled = true;
        tagReaderTimer.start();
    }
    else if (command.compareTo("stopstart") == 0)
    {
        //Stop start
        Log.info("Server stop start");
        fsm.StopStart_f();
        //DISABLE TAG READER
        tagReaderEnabled = false;
    }
    else if (command.compareTo("5beeps") == 0)
    {
        //TODO
        Log.info("5 beep mode");
    }
    else if (command.compareTo("3beeps") == 0)
    {
        //TODO
        Log.info("3 beep mode");
    }
    else if (command.compareTo("nobeeps") == 0)
    {
        //TODO
        Log.info("no beep mode");
    }
    else if (command.length() > 5 && command.substring(0, 5).compareTo("intvl") == 0)
    {
        //TODO
        Log.info("Interval %ld seconds", command.substring(5).toInt());
    }
    else
    {
        Log.error("Invalid server command");
    }

    Log.info("Memory used: %lu", System.freeMemory()); //TODO REMOVE
    return 0;
}

//GPS TIME IN UTC MODE (SHIFTED FOR LEAP SECONDS)
void setGPSOffset()
{
    Log.info("GPS Time set");
    detachInterrupt(GPSPPS); //detach GPS PPS pin interrupt, we have the time
    gps.getTimePulse();      //get time of last time pulse
    fsm.gps_weeks = gps.weeks;
    //later, to get the true time of an event, using millis() - (system time of GPS mark) + (GPS time of GPS mark)
    //gps_ms_offset is (GPS time of GPS mark) - (system time of GPS mark)
    fsm.gps_ms_offset = gps.ms - rtcOffset;
    fsm.GPSTimeAcquired();
    gps.powersave();
}

void gpsPPSCallback()
{
    rtcOffset = millis();
    //sets flag on GPS success to trigger state machine
    gpstimefix++;
}

/**
 * @brief Sets the time of the start interrupt in startdata.utc_millis
 * 
 */
void wandInterrupt()
{
    if (fsm.wandInterruptEnabled)
    {
        fsm.wand_ms = millis();
        startevent = true;
        fsm.wandInterruptEnabled = false;
    }
}

//------------------------------------------
//POWER MANAGEMENT STUFF

void powerButtonTimerCallback()
{
    powerOffCycleCount++;
    //if the power button pin is low, that means it is no longer being held
    if (!pinReadFast(POWER_BUTTON_PIN))
    {
        powerOffCycleCount = 0;
        powerButtonTimer.stopFromISR();
    }

    //if it's been enough time, turn off the system
    if (powerOffCycleCount >= (POWER_BUTTON_HOLD_TIME_MS / 50))
    {
        powerOffFlag = true;
        powerButtonTimer.stopFromISR();
    }
}

void powerButtonInterrupt()
{
    //start power button timer
    powerButtonTimer.startFromISR();
    powerOffCycleCount = 0;
}

void powerOff()
{
    detachInterrupt(POWER_BUTTON_PIN);
    Log.info("Turning off");
    //blank screen
    epd->powerup();
    epd->fullRefresh();
    epd->lcdCheck();

    //TODO: REPLACE WITH BATTERY POWER INDICATOR

    //turn off power
    digitalWrite(ACC_POWER_MOSFET, LOW);

    //disconnect from internet
    Particle.setDisconnectOptions(CloudDisconnectOptions().graceful(true).timeout(5000));
    Particle.disconnect();

    //some time to make sure the button is unpushed and internet disconnects
    delay(10000);

    //sleep
    SystemSleepConfiguration sleepconfig;
    sleepconfig.mode(SystemSleepMode::HIBERNATE);
    sleepconfig.gpio(POWER_BUTTON_PIN, RISING);
    System.sleep(sleepconfig);
}

static int lastBatteryState = 0;
void dispBatteryChg(int charge)
{
    //TODO FILL THIS IN
    //SHOW BATTERY STATE ON SCREEN

    //battery state is the same, don't update
    if (charge == lastBatteryState || charge < 0)
    {
        return;
    }
    lastBatteryState = charge;

    epd->powerup();
    char batteryscreen[2888];

    if (System.batteryState() == BATTERY_STATE_CHARGED || charge > 99)
    {
        epd->setOTPLUT();
        epd->sendImage(fullychargedIMG);
        epd->lcdCheck();
        epd->sleep();
        digitalWrite(ACC_POWER_MOSFET, LOW);
    }
    else
    {
        memset(batteryscreen, 0, 2888);
        int left = (charge / 10) % 10;
        int right = charge % 10;

        //helper arrays for numbers
        const char *numR_array[10] = {num0r, num1r, num2r, num3r, num4r, num5r, num6r, num7r, num8r, num9r};
        const char *numL_array[10] = {num0l, num1l, num2l, num3l, num4l, num5l, num6l, num7l, num8l, num9l};

        for (int i = 0; i < 2888; i++)
        {
            batteryscreen[i] = chargingIMG[i] & numL_array[left][i] & numR_array[right][i];
        }

        if (charge % 5 == 0)
        {
            //once every 5 updates, do a full screen refresh
            epd->setOTPLUT();
        }
        else
        {
            epd->setFastLUT();
        }
        epd->sendImage(batteryscreen);
        epd->lcdCheck();
    }
    epd->sleep();
}

void setup()
{
    //batteryFuelGauge.setAlertThreshold(10); //set 10% battery charge threshold
    Serial.begin(115200);
    pinMode(USB_PWR, INPUT);

    SPI.begin();
    epd = new Epaper(DISPLAYBUSY, DISPLAYRESET, DISPLAYDC, DISPLAYCS, 8000); //6750ms refresh time for default OTP
    epd->initIO();

    if (digitalRead(USB_PWR)) //USB is plugged in
    {
        chargingModeFlag = true;
        epd->powerup();
        epd->setOTPLUT();
        epd->fullRefresh();
        epd->lcdCheck();
        epd->sleep();

    }
    else //USB is not plugged in, go to race mode
    {
        chargingModeFlag = false;

        //turn on accessory power mosfet
        pinMode(ACC_POWER_MOSFET, OUTPUT);
        digitalWrite(ACC_POWER_MOSFET, HIGH);
        gps.begin(); //GPS start
        pinMode(GPSPPS, INPUT_PULLDOWN);
        attachInterrupt(GPSPPS, gpsPPSCallback, FALLING, 2);

        //turn off MFRC522 to save power for now
        pinMode(NFCRESET, OUTPUT);
        digitalWrite(NFCRESET, LOW);

        pinMode(POWER_BUTTON_PIN, INPUT_PULLDOWN);
        delay(50); //debounce delay
        //power on sequence
        while (millis() < POWER_BUTTON_HOLD_TIME_MS) //make sure the button is held down long enough
        {
            if (!digitalRead(POWER_BUTTON_PIN))
            {
         //       powerOff(); DEBUG
            }
        }

        //epaper
        epd->powerup();
        epd->setOTPLUT();
        epd->sendImage(loadingIMG);

        //subscribe to settings function
        Particle.function("startsettings", serverSettingsCallback);
        Particle.connect();

        //wait for display
        epd->lcdCheck();
        epd->sleep();

        //wait for internet
        waitUntil(Particle.connected);
        Particle.publishVitals(1h);

        pinMode(WAND_PIN, INPUT_PULLUP);
        attachInterrupt(WAND_PIN, wandInterrupt, FALLING, 2);

        //check that power button is released
        while (digitalRead(POWER_BUTTON_PIN));
        delay(500); //just to be safe for bounces
        //set up power off pin
        attachInterrupt(POWER_BUTTON_PIN, powerButtonInterrupt, RISING);
        Log.info("Race setup finished");
    }
}

int testcharge = 0;

void loop()
{
    if (gpstimefix > 3 && !gpstimeset)
    {
        setGPSOffset();
        gpstimefix++;
        gpstimeset = true;
    }
    if (startevent)
    {
        startevent = false;
        fsm.Wand();
    }

    if (tagReaderTimerFlag && tagReaderEnabled)
    {
        mfrc522.PCD_Init(); // start MFRC522
        delay(4);           // Optional delay. Some board do need more time after init to be ready, here to be safe
        if (mfrc522.PICC_IsNewCardPresent())
        {
            if (mfrc522.PICC_ReadCardSerial()) //select the new card
            {
                bibtemp = mfrc522.PICC_ReadBibWristband();
                if (bibtemp > 0)
                {
                    fsm.bib = bibtemp;
                    fsm.Tap();
                }
                else
                {
                    //TODO: MAYBE PLAY SOUND FOR A BAD WRISTBAND?
                }
            }
        }
        tagReaderTimerFlag = false;
        pinMode(NFCRESET, OUTPUT);
        digitalWrite(NFCRESET, LOW); //turn off MFRC522
        //start delay timer for tagReaderTimerFlag
        tagReaderTimer.start();
    }

    if (powerOffFlag)
    {
        powerOff();
    }

    //charging mode
    if (chargingModeFlag)
    {
        Log.info("Charge state: %d", (int)System.batteryCharge());

        //go to sleep if battery is charged
        if (System.batteryState() == BATTERY_STATE_CHARGED)
        {
            //sleep
            dispBatteryChg(100);
            Log.info("Battery charged, shutting down");
            // SystemSleepConfiguration sleepconfig;
            // sleepconfig.mode(SystemSleepMode::HIBERNATE);
            // sleepconfig.gpio(POWER_BUTTON_PIN, RISING);
            // System.sleep(sleepconfig); COMMENTED DEBUG
        }
        else
        {
            dispBatteryChg((int)System.batteryCharge());
        }
        //if USB disconnects, power off
        if (!digitalRead(USB_PWR))
        {
            powerOff();
        }
        delay(10000);
    }
    //normal operation, check battery state and run particle.process
    else
    {
        //check battery state
        //TODO TEST
        if (System.batteryCharge() < 0.2) //TODO MAYBE ADJUST
        {
            epd->powerup();
            epd->setOTPLUT();
            epd->sendImage(deadbatteryIMG);
            epd->lcdCheck();
            epd->sleep();
            powerOff();
        }
        Particle.process();
    }
}