#ifndef EPAPER
#define EPAPER

/**
 * @brief Epaper class
 * In general, functions return 0 on success, -1 on error
 */
class Epaper
{
    public:
        Epaper(int busyPin, int resetPin, int dataCommandPin, int chipSelect, int timeoutms);
        void initIO();
        int powerup();
        int sleep();
        int sendImage(char* olddata, char* newdata); //DATA SIZE MUST BE 2888
        int sendImage(char* image);
        int sendImage(char* olddata, const char* newdata); //DATA SIZE MUST BE 2888
        int sendImage(const char* image);
        int sendPartialImage(unsigned int x_start, unsigned int x_end, unsigned int y_start, unsigned int y_end, const char *old_data, const char *new_data);
        int sendPartialImage(unsigned int x_start, unsigned int x_end, unsigned int y_start, unsigned int y_end, const char *new_data);
        int fullRefresh();
        void setFastLUT();
        int lcdCheck();
        void setOTPLUT();
        void loadFastLUT();
        void sendCmd(char cmd);
        void sendData(char data);
        unsigned int timeout;
        

    private:
        char lastimagebuffer[2888];
        int busy;
        int reset;
        int dc;
        int cs;
        int lutstatus = 0;



//NOTE: THE PARTIAL WAVEFORM NUMBERS HAVE BEEN REPLACED
//THE MFG-PROVIDED PARTIAL WAVEFORM NUMBERS ARE AT THE BOTTOM OF THE FILE
        /////////////////////////////////////partial screen update LUT///////////////////////////////////////////
        const unsigned char lut_vcom1[44] ={
        0x00	,10	,0	,60	,10	,0x01,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00
        ,0x00	,0x00};
        const unsigned char lut_ww1[42] ={
        0x00	,10	,0	,60	,10	,0x01,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00};
        const unsigned char lut_bw1[42] ={
        0x5A	,10	,0	,60	,10	,0x01,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00	};
        const unsigned char lut_wb1[42] ={
        0x84	,10	,0	,60	,10	,0x01,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00	};
        const unsigned char lut_bb1[42] ={
        0x00	,10	,0	,60	,10	,0x01,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00	};

        /////////////////////////////////////full screen update LUT////////////////////////////////////////////
        const unsigned char lut_vcom[44] ={
        0x00	,0x08	,0x00	,0x00	,0x00	,0x02,	
        0x60	,0x28	,0x28	,0x00	,0x00	,0x01,	
        0x00	,0x14	,0x00	,0x00	,0x00	,0x01,	
        0x00	,0x12	,0x12	,0x00	,0x00	,0x01,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00	
        ,0x00	,0x00,					};
        const unsigned char lut_ww[42] ={	
        0x40	,0x08	,0x00	,0x00	,0x00	,0x02,	
        0x90	,0x28	,0x28	,0x00	,0x00	,0x01,	
        0x40	,0x14	,0x00	,0x00	,0x00	,0x01,	
        0xA0	,0x12	,0x12	,0x00	,0x00	,0x01,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	};
        const unsigned char lut_bw[42] ={	
        0x40	,0x17	,0x00	,0x00	,0x00	,0x02	,
        0x90	,0x0F	,0x0F	,0x00	,0x00	,0x03	,
        0x40	,0x0A	,0x01	,0x00	,0x00	,0x01	,
        0xA0	,0x0E	,0x0E	,0x00	,0x00	,0x02	,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00	,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00	,
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00	,					};
        const unsigned char lut_wb[42] ={	
        0x80	,0x08	,0x00	,0x00	,0x00	,0x02,	
        0x90	,0x28	,0x28	,0x00	,0x00	,0x01,	
        0x80	,0x14	,0x00	,0x00	,0x00	,0x01,	
        0x50	,0x12	,0x12	,0x00	,0x00	,0x01,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	};
        const unsigned char lut_bb[42] ={	
        0x80	,0x08	,0x00	,0x00	,0x00	,0x02,	
        0x90	,0x28	,0x28	,0x00	,0x00	,0x01,	
        0x80	,0x14	,0x00	,0x00	,0x00	,0x01,	
        0x50	,0x12	,0x12	,0x00	,0x00	,0x01,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
        0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	};

};









#endif


/*
/////////////////////////////////////partial screen update LUT///////////////////////////////////////////
const unsigned char lut_vcom1[] ={
0x00	,0x19	,0x01	,0x00	,0x00	,0x01,
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
0x00	,0x00	,0x00	,0x00	,0x00	,0x00
	,0x00	,0x00,					};
const unsigned char lut_ww1[] ={
0x00	,0x19	,0x01	,0x00	,0x00	,0x01,
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,};
const unsigned char lut_bw1[] ={
0x80	,0x19	,0x01	,0x00	,0x00	,0x01,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	};
const unsigned char lut_wb1[] ={
0x40	,0x19	,0x01	,0x00	,0x00	,0x01,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	};
const unsigned char lut_bb1[] ={
0x00	,0x19	,0x01	,0x00	,0x00	,0x01,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	
0x00	,0x00	,0x00	,0x00	,0x00	,0x00,	};
*/