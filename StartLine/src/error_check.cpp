#include "error_check.h"

//Macros
SerialLogHandler logHandler (LOG_LEVEL_ALL);

void fatalerror(int code) {
    if(code != 0) {
        //TODO SHOW ERROR CODE ON EPAPER DISPLAY
        Serial.print("Fatal Error: ");
        Serial.println(code); 
        
        while(1);
    }
}