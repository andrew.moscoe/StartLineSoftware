#include "fsm.h"

using namespace std;


//Start race event from server
void RaceTimer::GPSTimeAcquired() {
    BEGIN_TRANSITION_MAP			              			// - Current State -
        TRANSITION_MAP_ENTRY (ST_DISCONNECTED)              // ST_WAITGPS
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_DISCONNECTED
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_ENABLETAG
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_WAITINGTAP
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_STOPSTART
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_READYGO
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_TRIGGERED
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)                // ST_STOPSTARTGO
	END_TRANSITION_MAP(NULL)

}

void RaceTimer::StartRace()
{
	BEGIN_TRANSITION_MAP			              			// - Current State -
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)                // ST_WAITGPS
		TRANSITION_MAP_ENTRY (ST_ENABLETAG)					// ST_DISCONNECTED
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_ENABLETAG
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_WAITINGTAP
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_STOPSTART
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_READYGO
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_TRIGGERED
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)                // ST_STOPSTARTGO
	END_TRANSITION_MAP(NULL)
}

void RaceTimer::StopRace()
{
	BEGIN_TRANSITION_MAP			              			// - Current State -
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)                // ST_WAITGPS
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_DISCONNECTED
		TRANSITION_MAP_ENTRY (ST_DISCONNECTED)				// ST_ENABLETAG
		TRANSITION_MAP_ENTRY (ST_DISCONNECTED)				// ST_WAITINGTAP
		TRANSITION_MAP_ENTRY (ST_DISCONNECTED)				// ST_STOPSTART
        TRANSITION_MAP_ENTRY (ST_DISCONNECTED)				// ST_READYGO
        TRANSITION_MAP_ENTRY (ST_DISCONNECTED)				// ST_TRIGGERED
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)                // ST_STOPSTARTGO
	END_TRANSITION_MAP(NULL)
}

void RaceTimer::StopStart_f()
{
	BEGIN_TRANSITION_MAP			              			// - Current State -
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)                // ST_WAITGPS
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_DISCONNECTED
		TRANSITION_MAP_ENTRY (ST_STOPSTART)				    // ST_ENABLETAG
		TRANSITION_MAP_ENTRY (ST_STOPSTART)				    // ST_WAITINGTAP
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)			    // ST_STOPSTART
        TRANSITION_MAP_ENTRY (ST_STOPSTARTGO)			   	// ST_READYGO
        TRANSITION_MAP_ENTRY (ST_STOPSTART)			    	// ST_TRIGGERED
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)                // ST_STOPSTARTGO
	END_TRANSITION_MAP(NULL)
}

void RaceTimer::StartStart_f()
{
	BEGIN_TRANSITION_MAP			              			// - Current State -
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)                // ST_WAITGPS
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_DISCONNECTED
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_ENABLETAG
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_WAITINGTAP
		TRANSITION_MAP_ENTRY (ST_WAITINGTAP)				// ST_STOPSTART
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_READYGO
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_TRIGGERED
        TRANSITION_MAP_ENTRY (ST_READYGO)                   // ST_STOPSTARTGO
	END_TRANSITION_MAP(NULL)
}

void RaceTimer::Tap()
{
	BEGIN_TRANSITION_MAP			              			// - Current State -
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)                // ST_WAITGPS
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_DISCONNECTED
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_ENABLETAG
		TRANSITION_MAP_ENTRY (ST_READYGO)			    	// ST_WAITINGTAP
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_STOPSTART
        TRANSITION_MAP_ENTRY (ST_READYGO)		    		// ST_READYGO
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_TRIGGERED
	END_TRANSITION_MAP(NULL)
}

void RaceTimer::Wand()
{
	BEGIN_TRANSITION_MAP			              			// - Current State -
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)                // ST_WAITGPS
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_DISCONNECTED
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_ENABLETAG
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)			    // ST_WAITINGTAP
		TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_STOPSTART
        TRANSITION_MAP_ENTRY (ST_TRIGGERED)		        	// ST_READYGO
        TRANSITION_MAP_ENTRY (EVENT_IGNORED)				// ST_TRIGGERED
	END_TRANSITION_MAP(NULL)
}

void RaceTimer::ST_WaitGPS(EventData* data) {
    //do nothing
    Log.info("Waiting for GPS");
}

void RaceTimer::ST_Disconnected(EventData* data)
{
    Log.info("Disconnected state");
    epd->powerup();
    epd->setFastLUT();
    epd->sendImage(loadRaceIMG);
    ERRCHK(epd->lcdCheck());
    epd->sleep();
}

void RaceTimer::ST_EnableTag(EventData* data)
{
    Log.info("EnableTag state");
    InternalEvent(ST_WAITINGTAP);
}

void RaceTimer::ST_WaitingTap(EventData* data)
{
    Log.info("Waiting for tap state");
    epd->powerup();
    epd->setFastLUT();
    epd->sendImage(tapIMG);
    epd->lcdCheck();
    epd->sleep();
}

void RaceTimer::ST_StopStart(EventData* data)
{
    Log.info("Stop start state");
    wandInterruptEnabled = false;
    //Beep TODO
    epd->powerup();
    epd->setFastLUT();
    epd->sendImage(stopStartIMG);
    epd->lcdCheck();
    epd->sleep();
}

void RaceTimer::ST_StopStartGo(EventData* data)
{
    Log.info("Stop start go state");
    wandInterruptEnabled = false;
    //Beep TODO
    epd->powerup();
    epd->setFastLUT();
    epd->sendImage(stopStartIMG);
    epd->lcdCheck();
    epd->sleep();
}



void RaceTimer::ST_ReadyGo(EventData* data)
{
    if(bib < 1) {
        return;
    }

    Log.info("Ready screen state, bib %d", bib);
    //TODO BEEP
    epd->powerup();
    epd->setFastLUT();

    //decode bib to numbers
    int left = (bib/10)%10;
    int right = bib % 10;

    char bibscreen[2888];
    for(int i = 0; i < 2888; i++) {
         bibscreen[i] = numL_array[left][i] & numR_array[right][i] & goIMG[i];
    }

    epd->sendImage(bibscreen);
    epd->lcdCheck();
    epd->sleep();
    wandInterruptEnabled = true;
}

void RaceTimer::ST_Triggered(EventData* data)
{    
    //10 digits for seconds + 3 digits for ms + 3 digits for bib + "B"
    char serverdata[20];
    memset(serverdata, 0, 20); //make sure memory assigned is clear
    sprintf(serverdata, "B%uW%luT%lu", bib, gps_weeks, wand_ms+gps_ms_offset); //format B<bib>T<time in UTC ms>
    Log.info("Wand triggered. Data %s", serverdata);
    //send data to server
    Particle.publish("starttime", serverdata, PRIVATE);

    epd->powerup();
    epd->fullRefresh();
    delay(500);
    epd->lcdCheck();
    epd->sleep();
    InternalEvent(ST_WAITINGTAP);
}

