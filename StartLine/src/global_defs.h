#ifndef GLOBALDEFS
#define GLOBALDEFS

//PINS
#define NFCSS A0
#define NFCRESET A1
#define DISPLAYBUSY A2
#define DISPLAYRESET A3
#define DISPLAYDC A4
#define DISPLAYCS A5
#define POWER_BUTTON_PIN D8
#define USB_PWR D7
#define WAND_PIN D6
#define WAND_EN D5
#define ACC_POWER_MOSFET D4
#define SOUND_PIN D3
#define GPSPPS D2


#define RST_PIN NFCRESET // Configurable, see typical pin layout above
#define SS_PIN NFCSS     // Configurable, see typical pin layout above


//CONFIGURATION
#define USEGPS
#define POWER_BUTTON_HOLD_TIME_MS 3000

//GLOBALS
extern String name;
extern Epaper *epd;

//GLOBAL FUNCTIONS
void wandInterrupt();

#endif